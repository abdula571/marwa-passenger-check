import requests

class Bot(object):

    @staticmethod
    def make_and_run(token, upload):
        # Bot(token, upload).handle_text(upload["message"]["text"])
        pass
        

    def __init__(self, token, upload) -> None:
        self.token = token
        self.chat_id = upload["message"]["chat"]["id"]


    def handle_text(self, text):

        if text == "/start":
            self.send_message({
                "chat_id": self.chat_id,
                "text": "Hello!"
            })
            return
        



    def send_message(self, message):
        url = self.url_for_method("sendMessage")
        requests.post(url, json=message)


    def url_for_method(self, method):
        return f"https://api.telegram.org/bot{self.token}/{method}"

    