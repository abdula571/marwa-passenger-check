import json
import sys
import pyexcel
import os.path
from passenger_checker import PassengerChecker

excel_path = sys.argv[1]
pdf_path = sys.argv[2]

if not os.path.isfile(excel_path):
    exit("Файл: " + excel_path + " не найден")

passenger_checker = PassengerChecker(pdf_path)

records = pyexcel.get_array(file_name=excel_path, start_row=8)

found_quantity = 0
not_found_passengers = []

for row in records:
    f_name = row[2]
    s_name = row[1]
    
    passenger_checker.check(f_name, s_name)

print(json.dumps(passenger_checker.report(), indent=4, ensure_ascii=False))