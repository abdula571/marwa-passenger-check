from PyPDF2 import PdfFileReader

pdf_document = "example.pdf"  
with open(pdf_document, "rb") as filehandle:  
   pdf = PdfFileReader(filehandle)
   info = pdf.getDocumentInfo()
   pages = pdf.getNumPages()   
   print ("number of pages: %i" % pages)   
   page1 = pdf.getPage(0)
   print(page1.extract_text())