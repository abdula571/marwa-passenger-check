from distutils import text_file
from email.mime import base
import json
import os
import re
from sys import flags
import fitz
from glob import glob
from os import path

class PassengerChecker:
    
    def __init__(self, pdf_path) -> None:

        pdf_files = glob(pdf_path+"/*.pdf")

        if len(pdf_files) == 0:
            raise ValueError("PDF файлы не найдены по пути: " + pdf_path)

        self.pdf_path = pdf_path
        self.pdf_files = pdf_files
        
        self.checked_quantity = 0
        self.found_quantity = 0
        self.not_found = []
        self.history_map = {}

    def check(self, f_name, s_name):

        self.checked_quantity += 1
        pattern = re.compile(fr"[\s\S]*?. {f_name}\s+{s_name}", re.I | re.M)
        
        for pdf_file in self.pdf_files:
            
            basename = path.basename(pdf_file)
            self.history_map.setdefault(basename, {
                "pages": {},
                "found_quantity": 0
            })

            with fitz.open(pdf_file) as doc:
                for page in doc:

                    self.history_map[basename]["pages"].setdefault(page.number, [])

                    blocks = page.get_text("blocks")
                    for block in blocks:
                        quand = "-".join(str(x) for x in block[0:4])

                        if (pattern.match(block[4])) is not None and (quand not in self.history_map[basename]["pages"][page.number]):
                            self.found_quantity += 1
                            self.history_map[basename]["found_quantity"] += 1
                            self.history_map[basename]["pages"][page.number].append(quand)
                            return

        self.not_found.append(f"{s_name} {f_name}")
    
    def report(self):

        report = {
            "Количество проверенных пассажиров:": self.checked_quantity,
            "Количество найденных:": self.found_quantity,
            "Количество не найденных:": self.checked_quantity - self.found_quantity,
            "Отчет по файлам:": {}
        }

        if len(self.not_found) > 0:
            report["Не найденные пассажиры:"] = self.not_found

        for (file_name, file_data) in self.history_map.items():
            file = f"{self.pdf_path}/{file_name}"

            report["Отчет по файлам:"][file_name] = file_data["found_quantity"]
            
            with fitz.open(file) as doc:
                for (page_number, quads) in file_data["pages"].items():
                    page = doc.load_page(int(page_number))
                    for quad_string in quads:
                        quad = list(float(x) for x in quad_string.split("-"))
                        page.add_highlight_annot(quad)
                
                checked_files_folder = f"{self.pdf_path}/checked"

                if not path.exists(checked_files_folder):
                    os.makedirs(checked_files_folder)

                doc.save(f"{checked_files_folder}/{file_name}")
                    
        return report